# Desafio Edebê


## Introdução

Este é um desafio para testar seus conhecimentos de Backend e Frontend

O objetivo é avaliar a sua forma de estruturação e autonomia em decisões para construir algo escalável utilizando os Frameworks sugeridos na vaga aplicada.

## Case

A editora precisa disponibilizar um serviço para os alunos do Ensino Fundamental I, onde eles por meio desta aplicação terão a oportunidade de aprender os Continentes/Países/Capitais/Bandeiras de uma maneira divertida e intuitiva.

## Recursos

1. Desenvolver REST API importando os dados do projeto: http://www.oorsprong.org/websamples.countryinfo/CountryInfoService.wso
2. Realizar o projeto em um reposítório GIT da sua preferência e depois nos disponibilizar o link.


## API

USAR o PHP com framework LARAVEL em sua última versão.

## FRONTEND

Utilizar o Angular em sua última versão.

### Base e modelo de Dados:

Pode-se utilizar o banco que for mais conveniente.
Criar o modelo de dados a partir dos dados disponibilizados na API.

### Sistema do CRON

Para prosseguir com o desafio, precisaremos criar na API um sistema de atualização que vai importar os dados para a Base de Dados com a versão mais recente do [Country Info Service](http://www.oorsprong.org/websamples.countryinfo/CountryInfoService.wso) para popular com dados o banco de dados.

Escolher o formato que seja mais cômodo para importar todos os dados para a Base de Dados, o CountryInfoService tem os seguintes formatos:

- JSON 
- XML (default)

Ter em conta que:

- Todos os dados deverão ter os campos personalizados `imported_at` que armazenará a data de quando esse dado foi importado.


### BACKEND (REST API)


Na REST API ter no mínimo os seguintes endpoints:

   - `GET /countries/:countryID`: Obter a informação somente de um país da base de dados.
   - `GET /countries`: Listar todos os países da base de dados, adiconando informações de: continente, capital, moeda, línguas e bandeira. (todos esses estão disponibilizados na API)
   EX: http://www.oorsprong.org/websamples.countryinfo/CountryInfoService.wso/FullCountryInfo/JSON/debug?sCountryISOCode=BR


### FRONEND

Ter duas telas:

- uma exibindo uma lista de países ordenadas (alfabética) e paginadas.
- outra com as informações detalhada do país, inclusive mostrar a imagem da bandeira do país. 


### Extras

- **Diferencial 1** Executar o projeto usando Docker
- **Diferencial 2** Escrever um esquema de segurança utilizando `API KEY` nos endpoints. Ref: https://learning.postman.com/docs/sending-requests/authorization/#api-key ou utilizar o próprio Laravel-Passport.

## Readme do Repositório

- Deve conter o título do projeto
- Uma descrição de uma frase
- Como instalar e usar o projeto (instruções)

## Finalização

Após finalizado, enviar a url do repositório para análise.
